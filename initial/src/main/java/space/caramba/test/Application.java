package space.caramba.test;

import org.jgroups.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.ustaxi.app.messages.impl.billing.DriverFullPaycheckInfoRequestEvent;

import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.boot.autoconfigure.jms.JmsProperties.AcknowledgeMode.CLIENT;

/**
 * @author dmitriy_bayandin on 16.08.2016.
 */
@SpringBootApplication
@EnableJms
@EnableScheduling
public class Application{

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
    }

    @Autowired
    private ConnectionFactory connectionFactory;

    //@Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory  factory =
                new DefaultJmsListenerContainerFactory ();
        factory.setSessionAcknowledgeMode(CLIENT.getMode());
        factory.setConnectionFactory(connectionFactory);

        return factory;
    }

    @JmsListener(destination = "${queue}")
    public void receiveMessage(Object message) throws JMSException {
        System.out.println("Received <" + message + ">");
        if (message instanceof Message) {
            Message msg = (Message) message;
            System.out.println("msgId = " + msg.getJMSMessageID());
//            final String body = msg.getBody(String.class);
//            if (!msgToken.equals(body)) {
//                this.jmsTemplate.convertAndSend(outQueue, msg);
//            }
        }
        // processing
        // throw new JMSException("Some exception");
    }

    @Inject
    private JmsTemplate jmsTemplate;

    @Value("${outQueue}")
    private String outQueue;

    private final String msgToken = UUID.randomUUID().toString();

    private final AtomicInteger counter = new AtomicInteger(0);

    @Scheduled(cron = "0/3 * * * * *")
    public void createGreeting() {
        final DriverFullPaycheckInfoRequestEvent event = FastenMessages.createDriverFullPayCheckRequestEvent();
        System.out.println("Sending, seqId = " + event.getSequenceId());
        jmsTemplate.convertAndSend(outQueue, event);
        //jmsTemplate.setReceiveTimeout(100500L);
        //final Message message = jmsTemplate.receive(outQueue);
        //System.out.println("Received answer: " + message);
    }

}
