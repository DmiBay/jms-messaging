package space.caramba.test;

import org.jgroups.util.UUID;
import org.ustaxi.app.messages.impl.billing.DriverFullPaycheckInfoRequestEvent;

/**
 * @author dmitriy_bayandin on 19.08.2016.
 */
public class FastenMessages {

    private final static String seqId = UUID.randomUUID().toString();

    public static DriverFullPaycheckInfoRequestEvent createDriverFullPayCheckRequestEvent() {
        DriverFullPaycheckInfoRequestEvent event = new DriverFullPaycheckInfoRequestEvent();
        event.setDriverId(15916L);
        event.setPaycheckId(134347L);
        event.setSequenceId(seqId);
        return event;
    }


}
